package com.yzabeliesa.weatherweatherlang.data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.*
import com.yzabeliesa.weatherweatherlang.data.api.RemoteWeatherDataSource
import com.yzabeliesa.weatherweatherlang.data.db.LocalWeatherDataSource
import com.yzabeliesa.weatherweatherlang.data.model.*
import com.yzabeliesa.weatherweatherlang.util.FAKE_WEATHERS_LIST
import junitparams.JUnitParamsRunner
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.MockitoAnnotations

@RunWith(JUnitParamsRunner::class)
class WeatherRepositoryTest {

    @Rule
    @JvmField val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var mockLocalWeatherDataSource: LocalWeatherDataSource

    @Mock
    private lateinit var mockRemoteWeatherDataSource: RemoteWeatherDataSource

    private lateinit var weatherRepository: WeatherRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        weatherRepository = spy(
            WeatherRepository(
                mockRemoteWeatherDataSource,
                mockLocalWeatherDataSource,
                Dispatchers.Unconfined)
        )
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `getWeatherList success returns list of weathers`() = runBlocking {
        val fakeWeathers = FAKE_WEATHERS_LIST
        val weatherList = WeatherList(fakeWeathers)
        val response = Data.success(weatherList)
        whenever(mockRemoteWeatherDataSource.getWeatherList(anyString())).thenReturn(response)
        whenever(mockLocalWeatherDataSource.getAllWeathers()).thenReturn(MutableLiveData(fakeWeathers))

        val result = weatherRepository.getWeatherList()
        result.observeForever {  }

        assert(result.value?.status == Status.SUCCESS)
        assert(result.value == Data.success(fakeWeathers))
        verify(mockRemoteWeatherDataSource).getWeatherList(anyString())
        verify(mockLocalWeatherDataSource).insertWeathers(fakeWeathers)
    }

    @Test
    fun `getWeatherList preserve favorite status`() = runBlocking {
        val fakeWeathers = FAKE_WEATHERS_LIST.map { weather -> weather.copy(favorite = true) }
        val newWeathersList = FAKE_WEATHERS_LIST
        val response = Data.success(WeatherList(newWeathersList))
        whenever(mockRemoteWeatherDataSource.getWeatherList(anyString())).thenReturn(response)
        whenever(mockLocalWeatherDataSource.getAllWeathers()).thenReturn(MutableLiveData(fakeWeathers))

        val result = weatherRepository.getWeatherList()
        result.observeForever {  }

        val captor = argumentCaptor<List<Weather>>()
        verify(mockLocalWeatherDataSource).insertWeathers(captor.capture())
        captor.firstValue.forEach { newWeather ->
            val oldWeather = (fakeWeathers.find { it.id == newWeather.id })!!
            assert(newWeather.favorite == oldWeather.favorite)
        }
    }

    @Test
    fun `getWeatherList failed`() = runBlocking {
        val fakeWeathers = FAKE_WEATHERS_LIST
        val response = Data.error<WeatherList>(Error.GENERIC_ERROR)
        whenever(mockRemoteWeatherDataSource.getWeatherList(anyString())).thenReturn(response)
        whenever(mockLocalWeatherDataSource.getAllWeathers()).thenReturn(MutableLiveData(fakeWeathers))

        val result = weatherRepository.getWeatherList()
        result.observeForever {  }

        assert(result.value?.status == Status.ERROR)
        verify(mockLocalWeatherDataSource, times(0)).insertWeathers(fakeWeathers)
    }

    @Test
    fun `getWeatherDetails success`() = runBlocking {
        val weather = FAKE_WEATHERS_LIST[0]
        val response = Data.success(weather)
        whenever(mockRemoteWeatherDataSource.getWeatherDetails(anyString())).thenReturn(response)
        whenever(mockLocalWeatherDataSource.getWeather(anyLong())).thenReturn(MutableLiveData(weather))

        val result = weatherRepository.getWeatherDetails(anyLong())
        result.observeForever {  }

        assert(result.value?.status == Status.SUCCESS)
        assert(result.value == response)
        verify(mockRemoteWeatherDataSource).getWeatherDetails(anyString())
        verify(mockLocalWeatherDataSource).insertWeather(weather)
    }

    @Test
    fun `getWeatherDetails preserve favorite status`() = runBlocking {
        val fakeWeather = FAKE_WEATHERS_LIST[0].copy(favorite = true)
        val newWeather = FAKE_WEATHERS_LIST[0]
        val response = Data.success(newWeather)
        whenever(mockRemoteWeatherDataSource.getWeatherDetails(anyString())).thenReturn(response)
        whenever(mockLocalWeatherDataSource.getWeather(anyLong())).thenReturn(MutableLiveData(fakeWeather))

        val result = weatherRepository.getWeatherDetails(anyLong())
        result.observeForever {  }

        val captor = argumentCaptor<Weather>()
        verify(mockLocalWeatherDataSource).insertWeather(captor.capture())
        assert(captor.firstValue.favorite == fakeWeather.favorite)
    }

    @Test
    fun `getWeatherDetails failed`() = runBlocking {
        val fakeWeather = FAKE_WEATHERS_LIST[0]
        val response = Data.error<Weather>(Error.GENERIC_ERROR)
        whenever(mockRemoteWeatherDataSource.getWeatherDetails(anyString())).thenReturn(response)
        whenever(mockLocalWeatherDataSource.getWeather(anyLong())).thenReturn(MutableLiveData(fakeWeather))

        val result = weatherRepository.getWeatherDetails(anyLong())
        result.observeForever {  }

        assert(result.value?.status == Status.ERROR)
        verify(mockLocalWeatherDataSource, times(0)).insertWeather(fakeWeather)
    }

    @Test
    fun updateFavoriteStatus() = runBlocking {
        val weather = FAKE_WEATHERS_LIST[0]
        val newFavorite = !weather.favorite

        weatherRepository.updateFavoriteStatus(weather, newFavorite)
        val captor = argumentCaptor<Weather>()
        verify(mockLocalWeatherDataSource).insertWeather(captor.capture())
        assert(captor.firstValue.favorite == newFavorite)
    }
}