package com.yzabeliesa.weatherweatherlang.data.api

import com.yzabeliesa.weatherweatherlang.data.model.Error
import junitparams.JUnitParamsRunner
import junitparams.Parameters
import org.junit.Test

import org.junit.Before
import org.junit.runner.RunWith
import java.io.IOException
import java.lang.Exception

@RunWith(JUnitParamsRunner::class)
class ErrorHandlerTest {

    private lateinit var errorHandler: ErrorHandler

    @Before
    fun setUp() {
        errorHandler = ErrorHandler()
    }

    @Test
    @Parameters(
        "400,BAD_REQUEST",
        "401,UNAUTHORIZED",
        "403,FORBIDDEN",
        "404,NOT_FOUND",
        "422,UNPROCESSABLE_ENTITY",
        "502,GENERIC_ERROR",
        "503,GENERIC_ERROR"
    )
    fun `getError API error`(code: Int, expected: String) {
        val expectedError = Error.valueOf(expected)
        val actualError = errorHandler.getError(code)
        assert(actualError == expectedError)
    }

    @Test
    fun `getError IOException`() {
        val exception = IOException()
        val error = errorHandler.getError(exception)
        assert(error == Error.NO_INTERNET_CONNECTION)
    }

    @Test
    fun `getError exceptions`() {
        val exception = Exception("Any other excption")
        val error = errorHandler.getError(exception)
        assert(error == Error.GENERIC_ERROR)
    }

}