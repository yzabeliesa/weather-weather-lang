package com.yzabeliesa.weatherweatherlang.data.api

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.whenever
import com.yzabeliesa.weatherweatherlang.data.model.Error
import com.yzabeliesa.weatherweatherlang.data.model.Status
import com.yzabeliesa.weatherweatherlang.data.model.Weather
import com.yzabeliesa.weatherweatherlang.data.model.WeatherList
import com.yzabeliesa.weatherweatherlang.util.FAKE_WEATHERS_LIST
import junitparams.JUnitParamsRunner
import junitparams.Parameters
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Before
import org.junit.Test

import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

@RunWith(JUnitParamsRunner::class)
class RemoteWeatherDataSourceTest {

    @Mock
    private lateinit var mockApiService: WeatherApiService

    @Mock
    private lateinit var mockErrorHandler: ErrorHandler

    private lateinit var remoteWeatherDataSource: RemoteWeatherDataSource

    private val mockApiKey = "apiKey"

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        remoteWeatherDataSource = Mockito.spy(
            RemoteWeatherDataSource(mockApiService, mockApiKey, mockErrorHandler)
        )
    }

    @Test
    fun `getWeatherList success`() = runBlocking {
        val fakeWeathers = FAKE_WEATHERS_LIST
        val weatherList = WeatherList(weathers = fakeWeathers)
        val mockResponse = Response.success(weatherList)
        whenever(mockApiService.getWeatherList(eq(mockApiKey), anyString(), anyString()))
            .thenReturn(mockResponse)

        val result = remoteWeatherDataSource.getWeatherList(anyString())
        assert(result.status == Status.SUCCESS)
        assert(result.value?.weathers == fakeWeathers)
    }

    @Test
    @Parameters("401", "402", "403", "404", "502", "503")
    fun `getWeatherList catch API error`(code: Int) = runBlocking {
        val errorBody = "{}".toResponseBody()
        val mockResponse = Response.error<WeatherList>(code, errorBody)
        whenever(mockApiService.getWeatherList(eq(mockApiKey), anyString(), anyString()))
            .thenReturn(mockResponse)

        whenever(mockErrorHandler.getError(anyInt()))
            .thenReturn(Error.GENERIC_ERROR)

        val result = remoteWeatherDataSource.getWeatherList(anyString())
        assert(result.status == Status.ERROR)
    }

    @Test
    fun `getWeatherList catch exception`() = runBlocking {
        val exception = "Some exception"
        whenever(mockApiService.getWeatherList(eq(mockApiKey), anyString(), anyString())).then {
            throw Exception(exception)
        }
        whenever(mockErrorHandler.getError(any<Exception>()))
            .thenReturn(Error.GENERIC_ERROR)

        val result = remoteWeatherDataSource.getWeatherList(anyString())
        assert(result.status == Status.ERROR)
        assert(result.message == exception)
    }

    @Test
    fun `getWeatherDetails success`() = runBlocking {
        val id = FAKE_WEATHERS_LIST[0].id ?: 0
        val weather = FAKE_WEATHERS_LIST[0]
        val mockResponse = Response.success(weather)
        whenever(mockApiService.getWeatherDetails(eq(mockApiKey), anyString(), anyString()))
            .thenReturn(mockResponse)

        val result = remoteWeatherDataSource.getWeatherDetails(id.toString())
        assert(result.status == Status.SUCCESS)
        assert(result.value == weather)
    }

    @Test
    @Parameters("401", "402", "403", "404", "502", "503")
    fun `getWeatherDetails catch API error`(code: Int) = runBlocking {
        val errorBody = "{}".toResponseBody()
        val mockResponse = Response.error<Weather>(code, errorBody)
        whenever(mockApiService.getWeatherDetails(eq(mockApiKey), anyString(), anyString()))
            .thenReturn(mockResponse)

        whenever(mockErrorHandler.getError(anyInt()))
            .thenReturn(Error.GENERIC_ERROR)

        val result = remoteWeatherDataSource.getWeatherDetails(anyString())
        assert(result.status == Status.ERROR)
    }

    @Test
    fun `getWeatherDetails catch exception`() = runBlocking {
        val exception = "Some exception"
        whenever(mockApiService.getWeatherDetails(eq(mockApiKey), anyString(), anyString())).then {
            throw Exception(exception)
        }
        whenever(mockErrorHandler.getError(any<Exception>()))
            .thenReturn(Error.GENERIC_ERROR)

        val result = remoteWeatherDataSource.getWeatherDetails(anyString())
        assert(result.status == Status.ERROR)
        assert(result.message == exception)
    }

}