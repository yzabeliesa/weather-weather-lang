package com.yzabeliesa.weatherweatherlang.util

import com.yzabeliesa.weatherweatherlang.data.model.Weather

val FAKE_WEATHERS_LIST: List<Weather> = listOf(
    Weather(id = 1, city = "City1", weather = "Cloudy", temp = 1.0),
    Weather(id = 2, city = "City2", weather = "Rain", temp = 2.0),
    Weather(id = 3, city = "City3", weather = "Snow", temp = 3.0),
    Weather(id = 4, city = "City4", weather = "Sunny", temp = 4.0),
    Weather(id = 5, city = "City5", weather = "Hail", temp = 5.0)
)