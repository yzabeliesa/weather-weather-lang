package com.yzabeliesa.weatherweatherlang.ui.weatherdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.yzabeliesa.weatherweatherlang.R
import com.yzabeliesa.weatherweatherlang.data.model.Error
import com.yzabeliesa.weatherweatherlang.data.model.Status
import com.yzabeliesa.weatherweatherlang.data.model.Weather
import com.yzabeliesa.weatherweatherlang.ui.MainActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.weather_details_fragment.*
import kotlinx.android.synthetic.main.weather_list_fragment.*

@AndroidEntryPoint
class WeatherDetailsFragment : Fragment() {

    companion object {
        fun newInstance(id: Long) = WeatherDetailsFragment().apply {
            this.arguments = Bundle().apply {
                putLong("id", id)
            }
        }
    }

    private lateinit var viewModel: WeatherDetailsViewModel
    private var snackbar: Snackbar? = null
    private var id: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            id = it.getLong("id")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.weather_details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(WeatherDetailsViewModel::class.java)
        setupAppBar()
        setupObserver()
        setupToggleFavoriteButton()

        viewModel.setCityId(id ?: 0)
    }

    override fun onDestroyView() {
        (activity as? MainActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        setHasOptionsMenu(false)
        super.onDestroyView()
    }

    private fun setupAppBar() {
        (activity as? MainActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setHasOptionsMenu(true)
    }

    private fun setupObserver() {
        viewModel.getWeatherDetails().observe(viewLifecycleOwner) { data ->
            when(data.status) {

                Status.LOADING -> {
                    setLoadingState(
                        showProgress = true,
                        showContents = false
                    )
                }

                Status.SUCCESS -> {
                    setLoadingState(
                        showProgress = false,
                        showContents = true
                    )

                    data.value?.let { weather ->
                        bindWeatherData(weather)
                    }
                }

                Status.ERROR -> {
                    setLoadingState(
                        showProgress = false,
                        showContents = false
                    )

                    val message = data.message ?: if (data.error == Error.NO_INTERNET_CONNECTION)
                        getString(R.string.no_internet_connection)
                    else getString(R.string.generic_error)

                    showErrorSnackbar(message)
                }

            }
        }
    }

    private fun setupToggleFavoriteButton() {
        weatherDetailsIvFavorite.setOnClickListener { viewModel.toggleFavorite() }
    }

    private fun setLoadingState(showProgress: Boolean, showContents: Boolean) {
        weatherDetailsProgress.visibility = if (showProgress) View.VISIBLE else View.GONE
        val contentsVisibility = if (showContents) View.VISIBLE else View.GONE

        weatherDetailsTvWeather.visibility = contentsVisibility
        weatherDetailsTvCity.visibility = contentsVisibility
        weatherDetailsTvTemp.visibility = contentsVisibility
        weatherDetailsTvMinMaxTemp.visibility = contentsVisibility
        weatherDetailsIvFavorite.visibility = contentsVisibility
    }

    private fun bindWeatherData(weather: Weather) {
        weatherDetailsTvCity.text = weather.city

        val tempFormat = getString(R.string.temp_format_1f)
        weatherDetailsTvTemp.text = String.format(tempFormat, weather.temp ?: 0.0)

        val minMaxTempFormat = getString(R.string.temp_hi_low_format)
        weatherDetailsTvMinMaxTemp.text = String.format(
            minMaxTempFormat,
            weather.maxTemp ?: 0.0,
            weather.minTemp ?: 0.0)

        weatherDetailsTvWeather.text = weather.weather

        val drawableId =
            if (weather.favorite) R.drawable.ic_favorite
            else R.drawable.ic_favorite_outlined

        val drawable = ResourcesCompat.getDrawable(resources, drawableId, requireContext().theme)
        weatherDetailsIvFavorite.setImageDrawable(drawable)
    }

    private fun showErrorSnackbar(message: String) {
        snackbar?.dismiss()
        if (snackbar == null) {
            weatherDetailsRoot?.let {
                snackbar = Snackbar.make(it, message, Snackbar.LENGTH_LONG)
            }
        } else {
            snackbar?.apply {
                setText(message)
            }
        }
        snackbar?.show()
    }

}