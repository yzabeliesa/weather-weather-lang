package com.yzabeliesa.weatherweatherlang.ui.weatherlist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.yzabeliesa.weatherweatherlang.R
import com.yzabeliesa.weatherweatherlang.data.model.Weather
import com.yzabeliesa.weatherweatherlang.util.OnItemClickListener
import kotlinx.android.synthetic.main.item_weather.view.*

class WeatherAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val weathersList = ArrayList<Weather>()
    private var onItemClickListener: OnItemClickListener<Weather>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return WeatherViewHolder(
            LayoutInflater
                .from(context)
                .inflate(R.layout.item_weather, parent, false))
    }

    override fun getItemCount(): Int = this.weathersList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = this.weathersList[position]
        if (holder is WeatherViewHolder) {
            holder.bind(item)
            holder.view.setOnClickListener(createClickListener(item))
        }
    }

    fun getWeatherAt(position: Int): Weather? {
        if (position >= this.weathersList.size) return null
        return this.weathersList[position]
    }

    /**
     * Clears all weathers in current list and sets a new weathers list.
     */
    fun setWeathers(weathers: List<Weather>) {
        this.weathersList.clear()
        this.weathersList.addAll(weathers)
        notifyDataSetChanged()
    }

    /**
     * Sets the item click listener implementation.
     */
    fun setOnItemClickListener(listener: OnItemClickListener<Weather>) {
        this.onItemClickListener = listener
    }

    /**
     * Creates an item click listener for a weather item.
     */
    private fun createClickListener(weather: Weather): View.OnClickListener {
        return View.OnClickListener {
            this.onItemClickListener?.onItemClick(weather)
        }
    }

    /**
     * Defines view for weather.
     */
    class WeatherViewHolder(val view: View): RecyclerView.ViewHolder(view) {

        private val context = view.context
        private val resources = context.resources

        /**
         * Binds weather data to view.
         */
        fun bind(weather: Weather) {

            val cardBgColor = getBgColor(weather.temp ?: 0.0)
            view.weatherCardView.setCardBackgroundColor(cardBgColor)

            val drawable = if (weather.favorite) {
                val drawableId = R.drawable.ic_favorite
                ResourcesCompat.getDrawable(resources, drawableId, context.theme)
            } else null
            view.weatherIvFavorite.setImageDrawable(drawable)

            view.weatherTvCity.text = weather.city
            view.weatherTvWeather.text = weather.weather
            view.weatherTvTemp.text = String.format(
                resources.getString(R.string.temp_format_1f),
                weather.temp ?: 0.0)

        }

        /**
         * Obtains background color according to weather temperature.
         */
        private fun getBgColor(temp: Double): Int {
            val color = when {
                temp <= 0 -> R.color.freezing
                temp <= 15 -> R.color.cold
                temp <= 30 -> R.color.warm
                else -> R.color.hot
            }

            return ResourcesCompat.getColor(resources, color, context.theme)
        }
    }

}