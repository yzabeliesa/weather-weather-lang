package com.yzabeliesa.weatherweatherlang.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.yzabeliesa.weatherweatherlang.R
import com.yzabeliesa.weatherweatherlang.ui.weatherlist.WeatherListFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        attachInitialFragment()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        return if (id == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0)
            supportFragmentManager.popBackStack()
        else super.onBackPressed()
    }

    private fun attachInitialFragment() {
        val fragment = WeatherListFragment.newInstance()
        supportFragmentManager
            .beginTransaction()
            .add(R.id.rootView, fragment)
            .commit()
    }

}