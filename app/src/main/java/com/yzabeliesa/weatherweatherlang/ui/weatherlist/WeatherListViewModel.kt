package com.yzabeliesa.weatherweatherlang.ui.weatherlist

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.yzabeliesa.weatherweatherlang.data.model.Data
import com.yzabeliesa.weatherweatherlang.data.model.Weather
import com.yzabeliesa.weatherweatherlang.data.repository.WeatherRepository

class WeatherListViewModel @ViewModelInject constructor (
    private val weatherRepository: WeatherRepository
) : ViewModel() {

    fun getWeatherList(): LiveData<Data<List<Weather>>> = weatherRepository.getWeatherList()

}