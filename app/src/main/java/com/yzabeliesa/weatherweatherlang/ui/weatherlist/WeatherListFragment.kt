package com.yzabeliesa.weatherweatherlang.ui.weatherlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.yzabeliesa.weatherweatherlang.R
import com.yzabeliesa.weatherweatherlang.data.model.Data
import com.yzabeliesa.weatherweatherlang.data.model.Error
import com.yzabeliesa.weatherweatherlang.data.model.Status
import com.yzabeliesa.weatherweatherlang.data.model.Weather
import com.yzabeliesa.weatherweatherlang.ui.weatherdetails.WeatherDetailsFragment
import com.yzabeliesa.weatherweatherlang.util.OnItemClickListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.weather_list_fragment.*
import kotlinx.android.synthetic.main.weather_list_fragment.view.*

@AndroidEntryPoint
class WeatherListFragment : Fragment() {

    companion object {
        fun newInstance() = WeatherListFragment()
    }

    private lateinit var viewModel: WeatherListViewModel
    private lateinit var adapter: WeatherAdapter
    private var snackbar: Snackbar? = null
    private var observable: LiveData<Data<List<Weather>>>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.weather_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(WeatherListViewModel::class.java)
        setupObserver()
        setupRecyclerView(view)
        setupSwipeToRefresh(view)
    }

    private fun setupRecyclerView(view: View) {
        adapter = WeatherAdapter(view.context)
        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        view.weatherListRv.layoutManager = layoutManager
        view.weatherListRv.adapter = adapter

        adapter.setOnItemClickListener(object : OnItemClickListener<Weather> {
            override fun onItemClick(item: Weather) { onWeatherClicked(item) }
        })
    }

    private fun setupSwipeToRefresh(view: View) {
        view.weatherListRootRefresh.setOnRefreshListener{
            observable?.removeObservers(viewLifecycleOwner)
            setupObserver()
        }
    }

    private fun setupObserver() {
        observable = viewModel.getWeatherList()
        observable?.observe(viewLifecycleOwner) { data ->
            when(data.status) {
                Status.LOADING -> {
                    weatherListRootRefresh.isRefreshing = true
                }

                Status.SUCCESS -> {
                    weatherListRootRefresh.isRefreshing = false
                    data.value?.let { weathers ->
                        adapter.setWeathers(weathers)
                    }
                }

                Status.ERROR -> {
                    weatherListRootRefresh.isRefreshing = false
                    val message = data.message ?: if (data.error == Error.NO_INTERNET_CONNECTION)
                        getString(R.string.no_internet_connection)
                        else getString(R.string.generic_error)

                    showErrorSnackbar(message)
                }
            }
        }
    }

    private fun showErrorSnackbar(message: String) {
        snackbar?.dismiss()
        if (snackbar == null) {
            weatherListRootRefresh?.let {
                snackbar = Snackbar.make(it, message, Snackbar.LENGTH_LONG)
            }
        } else {
            snackbar?.apply {
                setText(message)
            }
        }
        snackbar?.show()
    }

    private fun onWeatherClicked(item: Weather) {
        val fragment = WeatherDetailsFragment.newInstance(item.id ?: 0)
        parentFragmentManager
            .beginTransaction()
            .replace(R.id.rootView, fragment)
            .addToBackStack(WeatherListFragment::class.java.name)
            .commit()
    }

}