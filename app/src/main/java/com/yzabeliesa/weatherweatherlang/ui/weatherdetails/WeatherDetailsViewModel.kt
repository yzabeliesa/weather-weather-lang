package com.yzabeliesa.weatherweatherlang.ui.weatherdetails

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.yzabeliesa.weatherweatherlang.data.model.Data
import com.yzabeliesa.weatherweatherlang.data.model.Status
import com.yzabeliesa.weatherweatherlang.data.model.Weather
import com.yzabeliesa.weatherweatherlang.data.repository.WeatherRepository
import kotlinx.coroutines.launch

class WeatherDetailsViewModel @ViewModelInject constructor (
    private val weatherRepository: WeatherRepository
) : ViewModel() {

    private val cityIdLiveData = MutableLiveData<Long>()
    private val weatherDetailsLiveData = cityIdLiveData.switchMap { cityId ->
        weatherRepository.getWeatherDetails(cityId)
    }

    fun setCityId(id: Long) {
        cityIdLiveData.value = id
    }

    fun getWeatherDetails(): LiveData<Data<Weather>> {
        return weatherDetailsLiveData
    }

    fun toggleFavorite() {
        val weatherData = weatherDetailsLiveData.value
        if (weatherData?.status == Status.SUCCESS) {
            weatherData.value?.let { weather ->
                viewModelScope.launch {
                    val favorite = !weather.favorite
                    weatherRepository.updateFavoriteStatus(weather, favorite)
                }
            }
        }
    }

}