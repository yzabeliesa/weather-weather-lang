package com.yzabeliesa.weatherweatherlang.di

import android.content.Context
import androidx.room.Room
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.yzabeliesa.weatherweatherlang.data.api.*
import com.yzabeliesa.weatherweatherlang.data.db.LocalWeatherDataSource
import com.yzabeliesa.weatherweatherlang.data.db.WeathersDatabase
import com.yzabeliesa.weatherweatherlang.data.model.Weather
import com.yzabeliesa.weatherweatherlang.data.model.WeatherList
import com.yzabeliesa.weatherweatherlang.data.repository.WeatherRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object WeathersModule {

    /*
    ============================
     Local weather dependencies
    ============================
    */
    @Singleton
    @Provides
    fun provideWeathersDatabase(@ApplicationContext appContext: Context): WeathersDatabase =
        Room.databaseBuilder(appContext, WeathersDatabase::class.java, "weathers")
            .fallbackToDestructiveMigration()
            .build()

    @Singleton
    @Provides
    fun provideLocalWeatherDataSource(db: WeathersDatabase) = db.weathersDao()

    /*
    =============================
     Remote weather dependencies
    =============================
    */
    @Singleton
    @Provides
    fun provideErrorHandler() = ErrorHandler()

    @Singleton
    @Provides
    fun provideWeatherTypeAdapter() = WeatherTypeAdapter()

    @Singleton
    @Provides
    fun provideWeatherListTypeAdapter() = WeatherListTypeAdapter()

    @Provides
    @Singleton
    fun provideGson(weatherTypeAdapter: WeatherTypeAdapter,
                    weatherListTypeAdapter: WeatherListTypeAdapter): Gson =
        GsonBuilder()
            .registerTypeAdapter(Weather::class.java, weatherTypeAdapter)
            .registerTypeAdapter(WeatherList::class.java, weatherListTypeAdapter)
            .setPrettyPrinting()
            .create()

    @Provides
    @Singleton
    fun provideGsonConverterFactory(gson: Gson): GsonConverterFactory =
        GsonConverterFactory.create(gson)

    @Singleton
    @Provides
    fun provideRetrofit(gsonConverterFactory: GsonConverterFactory) : Retrofit =
        Retrofit.Builder()
            .baseUrl("https://api.openweathermap.org")
            .addConverterFactory(gsonConverterFactory)
            .build()

    @Singleton
    @Provides
    fun provideWeatherApiService(retrofit: Retrofit): WeatherApiService =
        retrofit.create(WeatherApiService::class.java)

    @Singleton
    @Provides
    fun provideRemoteWeatherDataSource(apiService: WeatherApiService,
                                       errorHandler: ErrorHandler) =
        RemoteWeatherDataSource(
            apiService = apiService,
            apiKey = "5165a7dcb0e1b00be2b1101ee0513b06",
            errorHandler = errorHandler
        )

    /*
    ============
     Repository
    ============
    */
    @Singleton
    @Provides
    fun provideRepositoryCoroutineDispatcher() = Dispatchers.IO

    @Singleton
    @Provides
    fun provideWeatherRepository(remoteWeatherDataSource: RemoteWeatherDataSource,
                                 localWeatherDataSource: LocalWeatherDataSource,
                                 coroutineDispatcher: CoroutineDispatcher
    ) = WeatherRepository(
        remoteWeatherDataSource,
        localWeatherDataSource,
        coroutineDispatcher
    )

}