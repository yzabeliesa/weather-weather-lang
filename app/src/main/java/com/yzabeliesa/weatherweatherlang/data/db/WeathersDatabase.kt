package com.yzabeliesa.weatherweatherlang.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.yzabeliesa.weatherweatherlang.data.model.Weather

@Database(entities = [Weather::class], version = 1, exportSchema = false)
abstract class WeathersDatabase : RoomDatabase() {
    abstract fun weathersDao(): LocalWeatherDataSource
}