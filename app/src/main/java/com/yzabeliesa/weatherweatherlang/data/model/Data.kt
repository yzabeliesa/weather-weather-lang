package com.yzabeliesa.weatherweatherlang.data.model

/**
 * Wrapper around retrieved data that reflects its state.
 *
 * @param status The state of the data.
 * @param value The actual data.
 * @param error An @[Error] enum object that describes the error that occurred.
 */

data class Data<T> (
    val status: Status,
    val value: T?,
    val error: Error?,
    val message: String?
) {

    companion object {
        /**
         * Success data builder.
         */
        fun<T> success(value: T?): Data<T> {
            return Data(Status.SUCCESS, value, null, null)
        }

        /**
         * Error data builder.
         */
        fun<T> error(error: Error = Error.GENERIC_ERROR, message: String? = null): Data<T> {
            return Data(Status.ERROR, null, error, message)
        }

        /**
         * Loading data builder.
         */
        fun<T> loading(value: T? = null): Data<T> {
            return Data(Status.LOADING, value, null, null)
        }
    }

}