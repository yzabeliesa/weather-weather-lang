package com.yzabeliesa.weatherweatherlang.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yzabeliesa.weatherweatherlang.data.model.Weather

/**
 * Room DAO for persisting @[Weather] data.
 */

@Dao
interface LocalWeatherDataSource {

    @Query("SELECT * FROM weathers")
    fun getAllWeathers() : LiveData<List<Weather>>

    @Query("SELECT * FROM weathers WHERE id = :id")
    fun getWeather(id: Long): LiveData<Weather>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertWeathers(weathers: List<Weather>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertWeather(weather: Weather)

}