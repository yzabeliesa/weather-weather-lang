package com.yzabeliesa.weatherweatherlang.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import com.yzabeliesa.weatherweatherlang.data.api.RemoteWeatherDataSource
import com.yzabeliesa.weatherweatherlang.data.db.LocalWeatherDataSource
import com.yzabeliesa.weatherweatherlang.data.model.Data
import com.yzabeliesa.weatherweatherlang.data.model.Error
import com.yzabeliesa.weatherweatherlang.data.model.Status
import com.yzabeliesa.weatherweatherlang.data.model.Weather
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class WeatherRepository @Inject constructor (
    private val remoteWeatherDataSource: RemoteWeatherDataSource,
    private val localWeatherDataSource: LocalWeatherDataSource,
    private val coroutineDispatcher: CoroutineDispatcher
) {

    /**
     * Fetches the weather data of Manila, Prague and Seoul as a list of @[Weather] objects.
     *
     * @return An observable @[LiveData] that fetches the weather list.
     */
    fun getWeatherList(): LiveData<Data<List<Weather>>> = fetchData(
        { localWeatherDataSource.getAllWeathers() },
        { remoteWeatherDataSource.getWeatherList("1701668,3067696,1835848") },
        { oldData, newData ->
            val weatherList = newData.weathers ?: listOf()
            val newWeathers = weatherList.map { weather ->
                val old = oldData?.find { it.id == weather.id }
                val new = loadFavoriteStatus(old, weather)
                new
            }
            localWeatherDataSource.insertWeathers(newWeathers)
        }
    )

    /**
     * Fetches the weather details of the city with the given ID.
     *
     * @param id Specific city ID.
     * @return An observable @[LiveData] that fetches the weather details.
     */
    fun getWeatherDetails(id: Long): LiveData<Data<Weather>> = fetchData(
        { localWeatherDataSource.getWeather(id) },
        { remoteWeatherDataSource.getWeatherDetails(id.toString()) },
        { oldData, newData ->
            val weather = loadFavoriteStatus(oldData, newData)
            localWeatherDataSource.insertWeather(weather)
        }
    )

    /**
     * Changes the favorite status of a weather.
     */
    suspend fun updateFavoriteStatus(weather: Weather, favorite: Boolean) {
        withContext(coroutineDispatcher) {
            val newWeather = weather.copy(favorite = favorite)
            localWeatherDataSource.insertWeather(newWeather)
        }
    }

    /**
     * Strategy for fetching data and returning an observable @[LiveData] that reflects the state
     * and can transform the data type. Data is fetched both locally and remotely, with the remote
     * data considered as "new" data. The new data is then used to update the local data.
     *
     * @param fetchLocal Fetches the data from a local source and returns it as an observable
     * @[LiveData] object.
     * @param fetchRemote Fetches the data from a remote source and returns it as a @[Data] object.
     * @param saveFetchedData The operation performed to save data from the remote source to the
     * local source.
     */
    private fun<T,R> fetchData (
        fetchLocal: () -> LiveData<T>,
        fetchRemote: suspend () -> Data<R>,
        saveFetchedData: suspend (oldData: T?, newData: R) -> Unit
    ): LiveData<Data<T>> = liveData(coroutineDispatcher) {
        emit(Data.loading<T>())

        val localData = fetchLocal()
        emitSource(localData.map { Data.loading(it) })

        val remoteData = fetchRemote()
        if (remoteData.status == Status.SUCCESS) {
            // Success
            remoteData.value?.let { data: R ->
                saveFetchedData(localData.value, data)
            }
            emitSource(localData.map { Data.success(it) })
        } else emit(Data.error<T>(
            error = remoteData.error ?: Error.GENERIC_ERROR,
            message = remoteData.message
        )) // Error
    }

    /**
     * Preserves the favorite status of an existing @[Weather] object.
     */
    private fun loadFavoriteStatus(old: Weather?, new: Weather): Weather {
        var weather = new
        old?.let {
            weather = new.copy(
                favorite = it.favorite
            )
        }
        return weather
    }

}