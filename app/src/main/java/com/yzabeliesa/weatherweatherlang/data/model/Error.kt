package com.yzabeliesa.weatherweatherlang.data.model

enum class Error {
    GENERIC_ERROR,
    NOT_FOUND,
    BAD_REQUEST,
    UNAUTHORIZED,
    FORBIDDEN,
    UNPROCESSABLE_ENTITY,
    NO_INTERNET_CONNECTION
}