package com.yzabeliesa.weatherweatherlang.data.api

import com.yzabeliesa.weatherweatherlang.data.model.Data
import com.yzabeliesa.weatherweatherlang.data.model.Error
import com.yzabeliesa.weatherweatherlang.data.model.Weather
import com.yzabeliesa.weatherweatherlang.data.model.WeatherList
import org.json.JSONObject
import retrofit2.Response
import javax.inject.Inject

class RemoteWeatherDataSource @Inject constructor (
    private val apiService: WeatherApiService,
    private val apiKey: String,
    private val errorHandler: ErrorHandler
) {

    /**
     * Obtains weathers list based on given city IDs.
     *
     * @param cityIds IDs of cities whose weathers will be obtained, separated by a comma.
     * @return @[WeatherList] wrapped in a @[Data] object.
     */
    suspend fun getWeatherList(cityIds: String): Data<WeatherList> {
        return executeRequest {
            apiService.getWeatherList(
                apiKey = apiKey,
                cityIds = cityIds
            )
        }
    }

    /**
     * Obtains weather details of city with given ID.
     *
     * @param id Specific city ID.
     * @return @[Weather] details wrapped in a @[Data] object.
     */
    suspend fun getWeatherDetails(id: String): Data<Weather> {
        return executeRequest {
            apiService.getWeatherDetails(
                apiKey = apiKey,
                cityId = id
            )
        }
    }

    /**
     * Executes the request and wraps the result in a @[Data] object. In case of an error, the
     * corresponding @[Error] wrapped in a @[Data] object is returned.
     *
     * @param request The request to execute.
     * @return Response wrapped in a @[Data] object.
     */
    private suspend fun <T> executeRequest(request: suspend () -> Response<T>): Data<T> {
        try {
            // Execute request
            val response = request()

            // Return success
            if (response.isSuccessful) {
                val body = response.body()
                body?.let {
                    return Data.success(it)
                }
            }

            // Return error
            val code = response.code()
            var error = errorHandler.getError(code)

            // Get message from error body if any
            var message: String? = null
            try {
                val errorBody = response.errorBody()?.string()
                val errorJson = JSONObject( errorBody ?: "")
                message = errorJson.getString("message")
            } catch (e: Exception) {
                error = Error.GENERIC_ERROR
            }
            return Data.error(error, message)

        } catch (e: Exception) {
            // Return error
            val error = errorHandler.getError(e)
            val message = if (error == Error.NO_INTERNET_CONNECTION) null else e.message
            return Data.error(error, message)
        }
    }

}