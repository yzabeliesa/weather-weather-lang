package com.yzabeliesa.weatherweatherlang.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "weathers")
data class Weather (
    @PrimaryKey val id: Long? = null,
    val city: String? = null,
    val weather: String? = null,
    val temp: Double? = null,
    val minTemp: Double? = null,
    val maxTemp: Double? = null,
    val favorite: Boolean = false
)