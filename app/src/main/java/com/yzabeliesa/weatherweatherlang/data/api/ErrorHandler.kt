package com.yzabeliesa.weatherweatherlang.data.api

import com.yzabeliesa.weatherweatherlang.data.model.Error
import java.io.IOException

class ErrorHandler {

    /**
     * Returns the corresponding @[Error] object based on the thrown exception.
     */
    fun getError(exception: Exception): Error {
        return when (exception) {
            is IOException -> Error.NO_INTERNET_CONNECTION
            else -> Error.GENERIC_ERROR
        }
    }

    /**
     * Returns the corresponding @[Error] object based on the API error code.
     */
    fun getError(code: Int): Error {
        return when(code) {
            400 -> Error.BAD_REQUEST
            401 -> Error.UNAUTHORIZED
            403 -> Error.FORBIDDEN
            404 -> Error.NOT_FOUND
            422 -> Error.UNPROCESSABLE_ENTITY
            else -> Error.GENERIC_ERROR
        }
    }

}