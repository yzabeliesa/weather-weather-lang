package com.yzabeliesa.weatherweatherlang.data.model

enum class Status {
    SUCCESS,
    LOADING,
    ERROR
}