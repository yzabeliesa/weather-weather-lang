package com.yzabeliesa.weatherweatherlang.data.api

import com.yzabeliesa.weatherweatherlang.data.model.Weather
import com.yzabeliesa.weatherweatherlang.data.model.WeatherList
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApiService {

    @GET("/data/2.5/group")
    suspend fun getWeatherList(@Query("appid") apiKey: String,
                       @Query("id") cityIds: String,
                       @Query("units") units: String = "metric"): Response<WeatherList>

    @GET("/data/2.5/weather")
    suspend fun getWeatherDetails(@Query("appid") apiKey: String,
                          @Query("id") cityId: String,
                          @Query("units") units: String = "metric"): Response<Weather>

}