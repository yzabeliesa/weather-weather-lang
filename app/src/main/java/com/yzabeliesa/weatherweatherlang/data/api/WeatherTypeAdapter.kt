package com.yzabeliesa.weatherweatherlang.data.api

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.yzabeliesa.weatherweatherlang.data.model.Weather
import java.lang.reflect.Type

class WeatherTypeAdapter : JsonDeserializer<Weather> {

    /**
     * Deserializes a @[JsonElement] into a @[Weather] object.
     */
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Weather {

        val jsonObject = json?.asJsonObject

        jsonObject?.let {
            return Weather(
                id = it.get("id").asLong,
                city = it.get("name").asString,
                weather = it.getAsJsonArray("weather")[0]
                    .asJsonObject
                    .get("main")
                    .asString,
                minTemp = it.getAsJsonObject("main")
                    .get("temp_min")
                    .asDouble,
                maxTemp = it.getAsJsonObject("main")
                    .get("temp_max")
                    .asDouble,
                temp = it.getAsJsonObject("main")
                    .get("temp")
                    .asDouble
            )
        }

        // Return if jsonObject is null
        return Weather()
    }

}