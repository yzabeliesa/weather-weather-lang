package com.yzabeliesa.weatherweatherlang.data.api

import com.google.gson.JsonArray
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.yzabeliesa.weatherweatherlang.data.model.Weather
import com.yzabeliesa.weatherweatherlang.data.model.WeatherList
import java.lang.reflect.Type

class WeatherListTypeAdapter : JsonDeserializer<WeatherList> {

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): WeatherList {

        val jsonObject = json?.asJsonObject

        jsonObject?.let {
            val listJsonArray = it.get("list").asJsonArray
            val weatherList = deserializeWeatherList(context, listJsonArray)
            return WeatherList(weatherList)
        }

        // Return if jsonObject is null
        return WeatherList()
    }

    /**
     * Deserializes a @[JsonArray] into a list of @[Weather] objects.
     */
    fun deserializeWeatherList(context: JsonDeserializationContext?,
                               jsonArray: JsonArray) : List<Weather> {

        val weatherList: MutableList<Weather> = mutableListOf()

        for (element in jsonArray) {
            val deserialized = context?.deserialize<Weather>(element, Weather::class.java)
            deserialized?.let { weather ->
                weatherList.add(weather)
            }
        }

        return weatherList
    }

}