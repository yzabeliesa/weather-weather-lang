package com.yzabeliesa.weatherweatherlang.util

interface OnItemClickListener<T> {

    fun onItemClick(item: T)

}